#!/bin/bash

# Usage: ./mock_script.sh arg
#        arg: txt file

cat $1 | grep -Eo '\w+' | grep -iw 'de' | wc -l
